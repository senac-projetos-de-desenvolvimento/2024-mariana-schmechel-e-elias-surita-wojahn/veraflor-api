import express from 'express'
import dotenv from 'dotenv';
import { sequelize } from './databases/conecta.js';
import { Admnistrador } from './models/Administrador.js';
import { Produto } from './models/Produto.js';
import { Dica } from './models/Dica.js';
import cors from "cors";
import routes from './routes.js';
import { Cliente } from './models/Cliente.js';
import { Carrinho } from './models/Carrinho.js';
import { CarrinhoItem } from './models/Carrinho_Item.js';
import { Endereco } from './models/Endereco.js';
import { Pedido } from './models/Pedido.js';
import './associations.js'; 

const app = express()
const port = process.env.PORT || 3004;

app.use(express.json())
app.use(cors());
app.use('/public', express.static('public')) // Serve arquivos estáticos da pasta 'public'
app.use(routes)

async function conecta_db() {
  try {
    await sequelize.authenticate();
    console.log('Conexão com o banco de dados realizada com sucesso');
    await Admnistrador.sync()
    await Cliente.sync({ alter: true})
    await Endereco.sync({ alter: true})
    await Produto.sync()
    await Dica.sync()
    await Carrinho.sync()
    await CarrinhoItem.sync()
    await Pedido.sync()
  } catch (error) {
    console.error('Erro na conexão com o banco:', error);
  }
}
conecta_db();

app.get('/', (req, res) => {
  res.send('Veraflor Garden')
})

app.listen(port, () => {
  console.log(`Servidor rodando na porta ${port}`)
})

