import multer from 'multer';
import path from 'path';

// Configuração do multer para salvar o arquivo com um nome único
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, './public/upload');
    },
    filename: (req, file, cb) => {
        const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9);
        const ext = path.extname(file.originalname);
        const baseName = path.basename(file.originalname, ext);
        cb(null, `${baseName}-${uniqueSuffix}${ext}`);
    }
});

// Validar a extensão do arquivo
const fileFilter = (req, file, cb) => {
    const extensoesImg = ['image/png', 'image/jpg', 'image/jpeg'];
    
    if (extensoesImg.includes(file.mimetype)) {
        cb(null, true);
    } else {
        cb(new Error('Formato de arquivo não suportado. Apenas formatos PNG, JPG e JPEG são permitidos.'), false);
    }
}

const uploadImgProduto = multer({ 
    storage: storage,
    fileFilter: fileFilter 
});

export { uploadImgProduto };
