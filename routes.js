import { Router } from "express";
import { produtoCreate, produtoDestaque, produtoDestroy, produtoEdit, produtoIndex, produtoIndexFilter, produtoIndexSearch, produtoShowDestaque, produtoUpdate } from "./controllers/produtoController.js";
import { adminCreate, adminDestroy, adminIndex, adminUpdate } from "./controllers/administradorController.js";
import { loginAdmin, loginCliente } from "./controllers/loginController.js";
import { dicaCreate, dicaDestroy, dicaIndex, dicaIndexProduto, dicasUpdate } from "./controllers/dicaController.js";
import { clienteCreate, clienteDestroy, clienteIndex, clienteUpdate } from "./controllers/clienteController.js";
import { cancelarCarrinhosInativos, carrinhoAtivoIndex, carrinhoCreate, carrinhoDestroy, carrinhoIndex, carrinhoIndexCliente, obterTotalCarrinho } from "./controllers/carrinhoController.js";
import { alterarQuantidadeProduto, carrinhoItensCreate, carrinhoItensDestroy, carrinhoItensIndex, carrinhoItensIndexId, carrinhoItensUpdate } from "./controllers/carrinhoItensController.js";
import { enderecoCreate, enderecoIndex, enderecoIndexCliente, enderecoUpdate, enderecoUpdateCliente } from "./controllers/enderecoController.js";
import { cancelarPedido, consultarPedidoCompleto, criarPedido, criarPedidoPix, pedidoIndex, pedidoIndexAdmin, pedidoUpdateEntrega } from "./controllers/pedidoController.js";
import { consultarStatusPagamento, consultarStatusPagamentoPix, notificacaoPagSeguro } from "./controllers/pagseguroController.js";
import { verificaLogin } from "./middlewares/verificaLogin.js";

const router = Router();

router.get('/produtos/destaque', produtoShowDestaque)
router.get("/produtos/busca", produtoIndexSearch)
router.get("/produtos/lista/:id", produtoEdit)
router.get("/produtos/:loja", produtoIndex)
router.get("/produtos/disponibilidade/:loja", produtoIndexAdmin)
router.get('/produtos/:loja/filtro', produtoIndexFilter) 
router.post("/produtos", produtoCreate)
router.delete("/produtos/:id", produtoDestroy)
router.put("/produtos/:id", produtoUpdate)
router.patch("/produtos/destaque/:id", produtoDestaque)
router.patch("/produtos/disponibilidade/:id", produtoDisponivel)

router.get("/administradores", adminIndex)
router.post("/administradores/cadastro", adminCreate)
router.delete("/administradores/:id", adminDestroy)
router.put("/administradores/altera/:id", adminUpdate)

router.get("/clientes", clienteIndex)
router.post("/clientes/cadastro", clienteCreate)
router.delete("/clientes/:id", clienteDestroy)
router.put("/clientes/altera/:id", clienteUpdate)

router.get("/endereco", enderecoIndex)
router.get("/endereco/:cliente_id", enderecoIndexCliente)
router.post("/endereco/cadastro", enderecoCreate)
router.put("/endereco/altera/:id", enderecoUpdate)
router.patch("/endereco/alteraDados/:cliente_id", enderecoUpdateCliente)

router.post('/login', loginAdmin)
router.post('/clienteLogin', loginCliente)

router.get("/dicas", dicaIndex)
router.get('/dicas/produto/:produto_id', dicaIndexProduto)
router.post("/dicas", dicaCreate)
router.delete("/dicas/:id", dicaDestroy)
router.put("/dicas/altera/:id", dicasUpdate)

router.get("/carrinho", carrinhoIndex)
router.get("/carrinho/:cliente_id", carrinhoIndexCliente)
router.get("/carrinho/listaAtivos/:cliente_id", carrinhoAtivoIndex)
router.get("/carrinho/checkout/:id", obterTotalCarrinho)
router.patch("/carrinho/cancela", cancelarCarrinhosInativos)
router.post("/carrinho", carrinhoCreate)
router.delete("/carrinho/:id", carrinhoDestroy)

router.get("/itens", carrinhoItensIndex)
router.get("/itens/:carrinho_id", carrinhoItensIndexId)
router.post("/itens", carrinhoItensCreate)
router.patch("/itens/altera/:cliente_id/:produto_id", alterarQuantidadeProduto)
router.delete("/itens/:id", carrinhoItensDestroy)
router.put("/itens/altera/:id", carrinhoItensUpdate)

router.post("/criarPedido", criarPedido)
router.post("/checkoutPix", criarPedidoPix)
router.get("/pedidos/listar/:loja", pedidoIndexAdmin)
router.patch("/pedidos/entregue/:id", pedidoUpdateEntrega)
router.get("/pedidos/completo/:id", consultarPedidoCompleto)
router.delete("/pedidos/:id", cancelarPedido)
router.get("/pedidos/:cliente_id", pedidoIndex)

router.post('/pagseguro/notificacao', notificacaoPagSeguro);
router.put('/atualizaStatus/:pagSeguroOrderId', consultarStatusPagamento);
router.put('/atualizaPix/:pagSeguroOrderId', consultarStatusPagamentoPix);

export default router