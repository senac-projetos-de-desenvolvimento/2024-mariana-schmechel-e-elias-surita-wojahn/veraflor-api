import { Carrinho } from '../models/Carrinho.js';
import { Pedido } from '../models/Pedido.js';
import axios from 'axios';

export const notificacaoPagSeguro = async (req, res) => {
    try {
        const { id, status, reference_id } = req.body;

        if (!id || !status || !reference_id) {
            return res.status(400).send('Dados da notificação incompletos');
        }

        const pedido = await Pedido.findOne({ where: { pagseguro_order_id: id } });
        if (pedido) {
            const novoStatus = converterStatusPagSeguro(status);

            await pedido.update({ status: novoStatus });
            return res.status(200).send('OK');
        } else {
            return res.status(404).send('Pedido não encontrado');
        }
    } catch (error) {
        console.error('Erro ao processar notificação:', error.message);
        return res.status(500).send('Erro interno ao processar notificação');
    }
};

// Converte o status enviado pelo PagSeguro para o status interno do sistema
const converterStatusPagSeguro = (statusPagSeguro) => {
    switch (statusPagSeguro) {
        case 'PAID':
            return 'Pago';
        case 'WAITING_PAYMENT':
            return 'Aguardando Pagamento';
        case 'REFUNDED':
            return 'Reembolsado';
        case 'CANCELED':
            return 'Cancelado';
        case 'IN_ANALYSIS':
            return 'Em Análise';
        case 'DECLINED':
            return 'Recusado';
        default:
            return 'Desconhecido';
    }
};


export const consultarStatusPagamento = async (req, res) => {
    try {
        const { pagSeguroOrderId } = req.params; 
        const tokenSandbox = '3cee7aba-db07-4ea4-9b3f-3827aae397e4265cb686456f8330cb290c69a1dceee28551-315f-44e1-81db-3331713d0d98'; 

        const response = await axios.get(`https://sandbox.api.pagseguro.com/orders/${pagSeguroOrderId}`, {
            headers: {
                'Authorization': `Bearer ${tokenSandbox}`
            }
        });

        const cobranca = response.data.charges && response.data.charges[0];  
        if (!cobranca) {
            return res.status(400).json({ message: 'Nenhuma cobrança encontrada para o pedido' });
        }

        const statusPagSeguro = cobranca.status; 
        const novoStatus = atualizarStatusPagSeguro(statusPagSeguro);

        const pedido = await Pedido.findOne({ where: { pagseguro_order_id: pagSeguroOrderId } });

        if (pedido) {
            await pedido.update({ status: novoStatus });

            const carrinho = await Carrinho.findOne({ where: { id: pedido.carrinho_id } });
            if (carrinho) {
                let novoStatusCarrinho = null;
                if (novoStatus === 'Pago') {
                    novoStatusCarrinho = 'Concluído';
                } else if (novoStatus === 'Recusado') {
                    novoStatusCarrinho = 'Cancelado';
                }

                if (novoStatusCarrinho) {
                    await carrinho.update({ status: novoStatusCarrinho });
                }
            }

            return res.status(200).json({
                message: 'Status do pedido e do carrinho atualizados com sucesso',
                statusPedido: novoStatus,
                statusCarrinho: carrinho ? carrinho.status : 'Carrinho não encontrado'
            });
        } else {
            return res.status(404).json({ message: 'Pedido não encontrado' });
        }
    } catch (error) {
        console.error('Erro ao consultar status do pagamento:', error.response ? error.response.data : error.message);
        return res.status(500).json({ message: 'Erro ao consultar status do pagamento', error: error.message });
    }
};

const atualizarStatusPagSeguro = (statusPagSeguro) => {
    switch (statusPagSeguro) {
        case 'PAID':
            return 'Pago';
        case 'WAITING_PAYMENT':
            return 'Aguardando Pagamento';
        case 'REFUNDED':
            return 'Reembolsado';
        case 'PENDING':
            return 'Pendente';
        case 'CANCELED':
            return 'Cancelado';
        case 'IN_ANALYSIS':
            return 'Em Análise';
        case 'DECLINED':
            return 'Recusado';
        default:
            return 'Desconhecido';
    }
};

const mapearStatusPagSeguro = (statusPix) => {
    switch (statusPix) {
        case 'PAID':
            return 'Pago';
        case 'PENDING':
            return 'Pendente';
        case 'CANCELED':
            return 'Cancelado';
        default:
            return 'Desconhecido';
    }
};

export const consultarStatusPagamentoPix = async (req, res) => {
    try {
        const { pagSeguroOrderId } = req.params;  
        const tokenSandbox = '3cee7aba-db07-4ea4-9b3f-3827aae397e4265cb686456f8330cb290c69a1dceee28551-315f-44e1-81db-3331713d0d98';

        const response = await axios.get(`https://sandbox.api.pagseguro.com/orders/${pagSeguroOrderId}`, {
            headers: {
                'Authorization': `Bearer ${tokenSandbox}`
            }
        });

        const qrCode = response.data.qr_codes && response.data.qr_codes[0];
        const statusPix = qrCode && qrCode.arrangements.includes('PIX') ? 'PAID' : 'PENDING';

        const statusAtualizado = mapearStatusPagSeguro(statusPix);  

        const pedido = await Pedido.findOne({ where: { pagseguro_order_id: pagSeguroOrderId } });

        if (pedido) {
            await pedido.update({ status: statusAtualizado });

            const carrinho = await Carrinho.findOne({ where: { id: pedido.carrinho_id } });
            if (carrinho) {
                let novoStatusCarrinho = null;
                if (statusPix === 'PAID') {
                    novoStatusCarrinho = 'Concluído';
                } else if (statusPix === 'PENDING') {
                    novoStatusCarrinho = 'Aguardando Pagamento';
                } else if (statusPix === 'CANCELED') {
                    novoStatusCarrinho = 'Cancelado';
                }

                if (novoStatusCarrinho) {
                    await carrinho.update({ status: novoStatusCarrinho });
                }
            }

            return res.status(200).json({
                message: 'Status do pedido e do carrinho atualizados com sucesso',
                statusPedido: statusAtualizado,
                statusCarrinho: carrinho ? carrinho.status : 'Carrinho não encontrado'
            });
        } else {
            return res.status(404).json({ message: 'Pedido não encontrado' });
        }
    } catch (error) {
        console.error('Erro ao consultar status do pagamento PIX:', error.response ? error.response.data : error.message);
        return res.status(500).json({ message: 'Erro ao consultar status do pagamento PIX', error: error.message });
    }
};
