import { Produto } from '../models/Produto.js';
import multer from 'multer';
import { Op, fn, col, Sequelize } from 'sequelize';
const uploadImgProduto = multer({ dest: './public/upload/' });

export const produtoIndex = async (req, res) => {
  const { loja } = req.params;

  try {
    const produtos = await Produto.findAll({
      where: {
        loja,
        disponivel: true
      },
      order: [
        ['descricao', 'ASC']
      ]
    });
    res.status(200).json(produtos);
  } catch (error) {
    res.status(400).send(error);
  }
};

export const produtoIndexAdmin = async (req, res) => {
  const { loja } = req.params;

  try {
    const produtos = await Produto.findAll({
      where: {
        loja
      },
      order: [
        ['descricao', 'ASC']
      ]
    });
    res.status(200).json(produtos);
  } catch (error) {
    res.status(400).send(error);
  }
};


export const produtoIndexSearch = async (req, res) => {
  const { search } = req.query;
  let where = { disponivel: true };

  if (search) {
    where = {
      ...where,
      [Op.or]: [
        Sequelize.where(Sequelize.fn('unaccent', Sequelize.col('descricao')), {
          [Op.iLike]: `%${search}%`
        }),
        Sequelize.where(Sequelize.fn('unaccent', Sequelize.col('categoria')), {
          [Op.iLike]: `%${search}%`
        }),
        Sequelize.where(Sequelize.col('descricao'), { [Op.iLike]: `%${search}%` }),
        Sequelize.where(Sequelize.col('categoria'), { [Op.iLike]: `%${search}%` })
      ]
    };
  }

  try {
    const produtos = await Produto.findAll({ where });
    res.status(200).json(produtos);
  } catch (error) {
    res.status(400).send(error);
  }
};


export const produtoIndexFilter = async (req, res) => {
  const { ambiente, tamanho, tipo, categoria, maxPreco, page = 1, limit = 15 } = req.query;
  const { loja } = req.params;  
  
  let where = { loja, disponivel: true};

  if (ambiente) {
    where.ambiente = { [Op.in]: ambiente.split(',') };
  }
  if (tamanho) {
    where.tamanho = { [Op.in]: tamanho.split(',') };
  }
  if (tipo) {
    where.tipo = { [Op.in]: tipo.split(',') };
  }
  if (categoria) {
    where.categoria = { [Op.in]: categoria.split(',') };
  }
  if (maxPreco) {
    where.preco = { [Op.lte]: parseFloat(maxPreco) };
  }

  const offset = (page - 1) * limit;

  try {
    const { rows: produtos, count } = await Produto.findAndCountAll({
      where,
      limit: parseInt(limit),
      offset: parseInt(offset)
    });

    res.set('X-Total-Count', count.toString());
    res.set('Access-Control-Expose-Headers', 'X-Total-Count');
    res.status(200).json(produtos);
  } catch (error) {
    res.status(400).send(error);
  }
};


export const produtoEdit = async (req, res) => {
  const { id } = req.params

  try {
    const produto = await Produto.findByPk(id)
    res.status(200).json(produto)
  } catch (error) {
    res.status(400).send(error)
  }
}

export const produtoCreate = async (req, res) => {
  uploadImgProduto.single('imagem')(req, res, async (err) => {
    if (err instanceof multer.MulterError) {
      return res.status(400).json({ erro: true, message: "Erro ao fazer upload da imagem" });
    } else if (err) {
      return res.status(500).json({ erro: true, message: "Erro interno do servidor" });
    }

    const { codigo, descricao, preco, tipo, destaque, ambiente, categoria, tamanho, loja, adminId  } = req.body;
    const imagem = req.file ? req.file.filename : null;

    if (!codigo || !descricao || !preco || !tipo || !imagem || !destaque || !ambiente || !categoria || !tamanho || !loja || !adminId) {
      return res.status(400).json({ erro: true, message: "Erro... Informe todos os dados." });
    }

    try {
      const produto = await Produto.create({
        codigo,
        descricao,
        preco,
        tipo,
        imagem,
        destaque,
        ambiente,
        categoria,
        tamanho,
        loja,
        administrador_id: adminId
      });
      return res.status(201).json({ erro: false, message: "Produto criado com sucesso!", produto });
    } catch (error) {
      return res.status(400).json({ erro: true, message: "Erro ao criar o produto", error });
    }
  });
};

export const produtoUpdate = async (req, res) => {
  const upload = uploadImgProduto.single('imagem');
  upload(req, res, async (err) => {
    if (err) {
      if (err instanceof multer.MulterError) {
        return res.status(400).json({ erro: true, message: "Erro ao fazer upload da imagem" });
      }
      return res.status(500).json({ erro: true, message: "Erro interno do servidor" });
    }

    const { id } = req.params;
    const { codigo, descricao, preco, tipo, destaque, ambiente, categoria, tamanho, loja } = req.body;
    const imagem = req.file ? req.file.filename : req.body.imagem;

    if (!codigo || !descricao || !preco || !tipo || !destaque || !ambiente || !categoria || !tamanho || !loja) {
      return res.status(400).json({ erro: true, message: "Erro... Informe todos os dados." });
    }

    try {
      const [updated] = await Produto.update({
        codigo,
        descricao,
        preco,
        tipo,
        imagem,
        destaque,
        ambiente,
        categoria,
        tamanho,
        loja
      }, {
        where: { id }
      });
      if (updated) {
        const updatedProduto = await Produto.findOne({ where: { id } });
        return res.status(200).json({ produto: updatedProduto });
      }
      throw new Error('Produto não encontrado');
    } catch (error) {
      return res.status(500).json({ erro: true, message: "Erro ao atualizar o produto", error });
    }
  });
};

export const produtoDestroy = async (req, res) => {
  const { id } = req.params;
  try {
    const deletaProduto = await Produto.destroy({ where: { id } });
    res.status(200).json(deletaProduto);
  } catch (error) {
    res.status(400).send(error);
  }
};

export const produtoShowDestaque = async (req, res) => {
  try {
    const produtos = await Produto.findAll({
      where: {
        destaque: true,
      },
    });

    if (produtos.length === 0) {
      res.status(404).json({ message: 'Nenhum produto em destaque encontrado.' });
      return;
    }

    res.status(200).json(produtos);
  } catch (error) {
    res.status(500).send(error);
  }
}

export const produtoDestaque = async (req, res) => {
  const { id } = req.params

  try {
    const produto = await Produto.findByPk(id)
    await Produto.update({ destaque: !produto.destaque }, { where: { id } })
    res.status(200).json(produto)
  } catch (error) {
    res.status(400).send(error)
  }
}

// altera exibição do produto para o cliente conforme disponibilidade
export const produtoDisponivel = async (req, res) => {
  const { id } = req.params

  try {
    const produto = await Produto.findByPk(id)
    await Produto.update({ disponivel: !produto.disponivel }, { where: { id } })
    res.status(200).json(produto)
  } catch (error) {
    res.status(400).send(error)
  }
}