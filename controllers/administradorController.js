import { sequelize } from '../databases/conecta.js'
import { Admnistrador } from '../models/Administrador.js';

export const adminIndex = async (req, res) => {

  try {
    const administradores = await Admnistrador.findAll();
    res.status(200).json(administradores)
  } catch (error) {
    res.status(400).send(error)
  }
}

export const adminCreate = async (req, res) => {
  const { nome, email, senha } = req.body

  if ( !nome || !email || !senha ) {
    res.status(400).json({ id: 0, msg: "Erro... Informe todos os dados." })
    return
  }

  try {
    const administradores = await Admnistrador.create({
        nome, email, senha
    });
    res.status(201).json(administradores)
  } catch (error) {
    res.status(400).send(error)
  }
}

export const adminUpdate = async (req, res) => {
  const { id } = req.params
  const { nome, email, senha } = req.body

  if (!nome || !email || !senha) {
      res.status(400).json({ id: 0, msg: "Atenção! Preencha todos os campos!" })
      return
  }

  try {
      const alteraAdmin = await Admnistrador.update({
        nome, email, senha
      }, {
          where: { id }
      });
      res.status(200).json(alteraAdmin)
  } catch (error) {
      res.status(400).send(error)
  }
}

export const adminDestroy = async (req, res) => {
  const { id } = req.params
  try {
      const deletaAdmin= await Admnistrador.destroy({
          where: { id }
      });
      res.status(200).json(deletaAdmin)
  } catch (error) {
      res.status(400).send(error)
  }
}
