import { Pedido } from '../models/Pedido.js';
import { Carrinho } from '../models/Carrinho.js';
import { CarrinhoItem } from '../models/Carrinho_Item.js'
import { Produto } from '../models/Produto.js';
import { Cliente } from '../models/Cliente.js';
import { Endereco } from '../models/Endereco.js';
import axios from 'axios';
import { Op } from 'sequelize';

const tokenSandbox = '3cee7aba-db07-4ea4-9b3f-3827aae397e4265cb686456f8330cb290c69a1dceee28551-315f-44e1-81db-3331713d0d98';

export const criarPedido = async (req, res) => {
    try {
        const { clienteId, carrinhoId, dadosPagamento, deliveryOption } = req.body;

        const novoPedido = await Pedido.create({
            cliente_id: clienteId,
            carrinho_id: carrinhoId,
            total: 0,
            status: 'Pendente',
            forma_pagamento: 'Cartão de Crédito',
            forma_entrega: deliveryOption,
            entregue: false
        });

        const carrinho = await Carrinho.findOne({
            where: { id: carrinhoId },
            include: [{
                model: CarrinhoItem,
                as: 'carrinhoItens',
                include: [{ model: Produto, as: 'produto' }]
            }]
        });

        const cliente = await Cliente.findByPk(clienteId);
        const endereco = await Endereco.findOne({ where: { cliente_id: clienteId } });

        // Montar a lista de itens e calcular o total
        let valorTotal = 0;

        const itens = carrinho.carrinhoItens.map(item => {
            const valorItem = item.produto.preco * item.quantidade;
            valorTotal += valorItem;
            return {
                reference_id: `produto_${item.produto.id}`,
                name: item.produto.descricao,
                quantity: item.quantidade,
                unit_amount: item.produto.preco * 100 // Converter para centavos
            };
        });

        // Adicionar frete R$ 20,00 se o total for inferior a R$ 200,00
        let valorFrete = 0;
        if (deliveryOption === 'Entrega' && valorTotal < 200) {
            valorFrete = 20;
            valorTotal += valorFrete;
        }

        await novoPedido.update({ total: valorTotal });

        // Montar os dados para a API do PagSeguro
        const dadosPedidoPagSeguro = {
            reference_id: `pedido_${novoPedido.id}`,
            customer: {
                name: cliente.nome,
                email: cliente.email,
                tax_id: cliente.cpf.replace(/[^\d]+/g, ''),
                phones: [
                    {
                        country: '55',
                        area: cliente.area,
                        number: cliente.celular,
                        type: 'MOBILE'
                    }
                ]
            },
            items: itens,
            shipping: {
                address: {
                    street: endereco.endereco,
                    number: endereco.numero,
                    complement: endereco.complemento || '',
                    locality: endereco.bairro,
                    city: endereco.cidade,
                    region_code: endereco.estado,
                    country: 'BRA',
                    postal_code: endereco.cep.replace('-', '')
                }
            },
            notification_urls: ["https://veraflor.onrender.com/pagseguro/notificacao"], // URL para a qual o PagSeguro envia as notificações
            charges: [
                {
                    reference_id: `charge_${novoPedido.id}`,
                    description: 'Pagamento de produtos da Veraflor',
                    amount: {
                        value: valorTotal * 100, // Converter para centavos
                        currency: 'BRL'
                    },
                    payment_method: {
                        type: 'CREDIT_CARD',
                        installments: 1,
                        capture: true,
                        card: {
                            number: dadosPagamento.numeroCartao,
                            exp_month: dadosPagamento.mesExpiracao,
                            exp_year: dadosPagamento.anoExpiracao,
                            security_code: dadosPagamento.codigoSeguranca,
                            holder: {
                                name: dadosPagamento.nomeTitular
                            }
                        }
                    }
                }
            ]
        };

        // Enviar a requisição para o PagSeguro
        const response = await axios.post('https://sandbox.api.pagseguro.com/orders', dadosPedidoPagSeguro, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${tokenSandbox}`
            }
        });

        // Atualizar o pedido com o ID retornado pelo PagSeguro
        const pagSeguroOrderId = response.data.id;
        await novoPedido.update({
            pagseguro_order_id: pagSeguroOrderId,
            status: 'Aguardando Pagamento'
        });

        res.status(201).json({
            message: 'Pedido criado com sucesso',
            pedido: novoPedido,
            pagseguro_order_id: pagSeguroOrderId
        });
    } catch (error) {
        console.error('Erro ao criar pedido:', error.response ? error.response.data : error.message);
        res.status(500).json({ message: 'Erro ao criar pedido', error: error.message });
    }
};


export const criarPedidoPix = async (req, res) => {
    try {
        const { clienteId, carrinhoId, deliveryOption } = req.body;

        const novoPedido = await Pedido.create({
            cliente_id: clienteId,
            carrinho_id: carrinhoId,
            total: 0,
            status: 'Pendente',
            forma_pagamento: 'Pix',
            forma_entrega: deliveryOption,
            entregue: false
        });

        const carrinho = await Carrinho.findOne({
            where: { id: carrinhoId },
            include: [{
                model: CarrinhoItem,
                as: 'carrinhoItens',
                include: [{ model: Produto, as: 'produto' }]
            }]
        });

        console.log(carrinho.carrinhoItens);  // verificar os dados dos itens

        const cliente = await Cliente.findByPk(clienteId);
        const endereco = await Endereco.findOne({ where: { cliente_id: clienteId } });

        let valorTotal = 0;
        const itens = carrinho.carrinhoItens.map(item => {
            const valorItem = item.produto.preco * item.quantidade;
            valorTotal += valorItem;

            return {
                reference_id: `produto_${item.produto.id}`,
                name: item.produto.descricao,
                quantity: item.quantidade,
                unit_amount: item.produto.preco * 100
            };
        });

        // Adicionar frete R$ 20,00 se o total for inferior a R$ 200,00
        let valorFrete = 0;
        if (deliveryOption === 'Entrega' && valorTotal < 200) {
            valorFrete = 20;
            valorTotal += valorFrete;
        }

        await novoPedido.update({ total: valorTotal });

        // Cria o pedido via PIX com QR Code
        const dadosPedidoPix = {
            reference_id: `pedido_${novoPedido.id}`,
            customer: {
                name: cliente.nome,
                email: cliente.email,
                tax_id: cliente.cpf.replace(/[^\d]+/g, ''),
                phones: [
                    {
                        country: '55',
                        area: cliente.area,
                        number: cliente.celular,
                        type: 'MOBILE'
                    }
                ]
            },
            items: itens,
            qr_codes: [
                {
                    amount: {
                        value: valorTotal * 100
                    },
                    expiration_date: new Date(new Date().getTime() + 24 * 60 * 60 * 1000).toISOString() //expiração: 24 horas
                }
            ],
            shipping: {
                address: {
                    street: endereco.endereco,
                    number: endereco.numero,
                    complement: endereco.complemento || '',
                    locality: endereco.bairro,
                    city: endereco.cidade,
                    region_code: endereco.estado,
                    country: 'BRA',
                    postal_code: endereco.cep.replace('-', '')
                }
            },
            notification_urls: ["https://veraflor.onrender.com/pagseguro/notificacao"]
        };

        // Enviar a requisição para a API do PagSeguro
        const response = await axios.post('https://sandbox.api.pagseguro.com/orders', dadosPedidoPix, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${tokenSandbox}`
            }
        });

        // Atualizar o pedido com o ID retornado pelo PagSeguro e exibir o QR Code
        const pagSeguroOrderId = response.data.id;
        await novoPedido.update({
            pagseguro_order_id: pagSeguroOrderId,
            status: 'Aguardando Pagamento'
        });

        res.status(201).json({
            message: 'Pedido PIX criado com sucesso',
            pedido: novoPedido,
            pagseguro_order_id: pagSeguroOrderId,
            qr_codes: response.data.qr_codes // Retorna os dados do QR Code, como o link e texto
        });
    } catch (error) {
        console.error('Erro ao criar pedido PIX:', error.response ? error.response.data : error.message);
        res.status(500).json({ message: 'Erro ao criar pedido PIX', error: error.message });
    }
};

export const cancelarPedido = async (req, res) => {
    const { id } = req.params;

    try {
        const pedido = await Pedido.findOne({ where: { id } });

        if (!pedido) {
            return res.status(404).json({ message: 'Pedido não encontrado' });
        }

        if (pedido.status === 'Cancelado') {
            return res.status(400).json({ message: 'Este pedido já está cancelado' });
        }

        await pedido.update({ status: 'Cancelado' });

        const carrinho = await Carrinho.findOne({ where: { id: pedido.carrinho_id } });

        if (carrinho && carrinho.status !== 'Cancelado') {
            await carrinho.update({ status: 'Cancelado' });
        }

        res.status(200).json({
            message: 'Pedido e carrinho cancelados com sucesso',
            pedidoStatus: 'Cancelado',
            carrinhoStatus: carrinho ? carrinho.status : 'Carrinho não encontrado'
        });
    } catch (error) {
        console.error('Erro ao cancelar pedido:', error.message);
        res.status(500).json({ message: 'Erro ao cancelar pedido', error: error.message });
    }
};


export const pedidoIndex = async (req, res) => {
    const { cliente_id } = req.params;

    try {
        const pedidos = await Pedido.findAll({
            where: {
                cliente_id,
                pagseguro_order_id: {
                    [Op.ne]: null
                }
            },
            order: [
                ['createdAt', 'DESC']
            ],
        });
        res.status(200).json(pedidos);
    } catch (error) {
        res.status(400).send(error);
    }
};


export const pedidoIndexAdmin = async (req, res) => {
    const { loja } = req.params; 

    try {
        const pedidos = await Pedido.findAll({
            where: {
                status: {
                    [Op.or]: ['Pago', 'Cancelado'] // Filtra apenas pedidos relevantes
                }
            },
            include: [
                {
                    model: Carrinho,
                    as: 'carrinho',
                    required: true,
                    include: [
                        {
                            model: CarrinhoItem,
                            as: 'carrinhoItens',
                            required: true, 
                            include: [
                                {
                                    model: Produto,
                                    as: 'produto',
                                    where: { loja }, 
                                    attributes: ['descricao', 'preco', 'loja'] 
                                }
                            ]
                        }
                    ]
                }
            ],
            order: [['createdAt', 'ASC']]
        });

        res.status(200).json(pedidos);
    } catch (error) {
        console.error('Erro ao listar pedidos por loja:', error.message);
        res.status(500).json({ message: 'Erro ao listar pedidos por loja', error: error.message });
    }
};


export const pedidoUpdateEntrega = async (req, res) => {
    const { id } = req.params
    try {
        const alteraEntrega = await Pedido.findByPk(id)
        await Pedido.update({ entregue: !alteraEntrega.entregue }, { where: { id } })
        res.status(200).json(alteraEntrega)
    } catch (error) {
        res.status(400).send(error)
    }
};

export const consultarPedidoCompleto = async (req, res) => {
    const { id } = req.params;

    try {
        const pedido = await Pedido.findOne({
            where: { id },
            include: [
                {
                    model: Cliente,
                    as: 'cliente',
                    attributes: ['nome', 'cpf', 'celular'],
                    include: [
                        {
                            model: Endereco,
                            as: 'endereco',
                            attributes: ['endereco', 'numero', 'complemento', 'bairro', 'cidade', 'estado', 'cep']
                        }
                    ]
                },
                {
                    model: Carrinho,
                    as: 'carrinho',
                    include: [
                        {
                            model: CarrinhoItem,
                            as: 'carrinhoItens',
                            attributes: ['quantidade'],
                            include: [
                                {
                                    model: Produto,
                                    as: 'produto',
                                    attributes: ['descricao', 'preco']
                                }
                            ]
                        }
                    ]
                }
            ]
        });

        if (!pedido) {
            return res.status(404).json({ message: 'Pedido não encontrado' });
        }

        res.status(200).json(pedido);

    } catch (error) {
        console.error('Erro ao consultar pedido:', error.message);
        res.status(500).json({ message: 'Erro ao consultar pedido', error: error.message });
    }
};

