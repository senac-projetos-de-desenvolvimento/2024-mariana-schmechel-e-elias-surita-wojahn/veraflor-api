import { Carrinho } from "../models/Carrinho.js";
import { CarrinhoItem } from "../models/Carrinho_Item.js";

export const carrinhoItensIndex = async (req, res) => {

    try {
        const itens = await CarrinhoItem.findAll();
        res.status(200).json(itens)
    } catch (error) {
        res.status(400).send(error)
    }
}

export const carrinhoItensIndexId = async (req, res) => {
    const { carrinho_id } = req.params;

    try {
        const itens = await CarrinhoItem.findAll({
            where: {
                carrinho_id
            }
        });
        res.status(200).json(itens)
    } catch (error) {
        res.status(400).send(error)
    }
}


export const carrinhoItensCreate = async (req, res) => {
    const { quantidade, carrinho_id, produto_id } = req.body

    if (!quantidade || !carrinho_id || !produto_id) {
        res.status(400).json({ id: 0, msg: "Erro... Informe todos os dados." })
        return
    }

    try {
        const itens = await CarrinhoItem.create({
            quantidade, carrinho_id, produto_id
        });
        res.status(201).json(itens)
    } catch (error) {
        res.status(400).send(error)
    }
}

export const carrinhoItensUpdate = async (req, res) => {
    const { id } = req.params
    const { quantidade, carrinho_id, produto_id } = req.body

    if (!quantidade || !carrinho_id || !produto_id) {
        res.status(400).json({ id: 0, msg: "Atenção! Preencha todos os campos!" })
        return
    }

    try {
        const alteraItens = await CarrinhoItem.update({
            quantidade, carrinho_id, produto_id
        }, {
            where: { id }
        });
        res.status(200).json(alteraItens)
    } catch (error) {
        res.status(400).send(error)
    }
}


export const alterarQuantidadeProduto = async (req, res) => {
    const { cliente_id, produto_id } = req.params; 
    const { quantidade } = req.body; 

    if (!quantidade || quantidade <= 0) {
        return res.status(400).json({ message: 'Quantidade deve ser maior que zero.' });
    }

    try {
        // busca o carrinho ativo do cliente
        const carrinho = await Carrinho.findOne({
            where: {
                cliente_id,
                status: 'Ativo'
            }
        });

        if (!carrinho) {
            return res.status(404).json({ message: 'Carrinho ativo não encontrado para o cliente.' });
        }

        const carrinhoItem = await CarrinhoItem.findOne({
            where: {
                carrinho_id: carrinho.id,
                produto_id
            }
        });

        if (!carrinhoItem) {
            return res.status(404).json({ message: 'Produto não encontrado no carrinho.' });
        }

        carrinhoItem.quantidade = quantidade;
        await carrinhoItem.save();

        return res.status(200).json({
            message: 'Quantidade atualizada com sucesso!',
            carrinhoItem
        });
    } catch (error) {
        return res.status(500).json({ message: 'Erro ao atualizar a quantidade do item no carrinho.', error });
    }
};


export const carrinhoItensDestroy = async (req, res) => {
    const { id } = req.params
    try {
        const deletaItem = await CarrinhoItem.destroy({
            where: { id }
        });
        res.status(200).json(deletaItem)
    } catch (error) {
        res.status(400).send(error)
    }
}
