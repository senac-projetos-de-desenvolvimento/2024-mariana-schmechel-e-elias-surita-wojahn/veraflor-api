import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken'

import * as dotenv from 'dotenv'
import { Admnistrador } from '../models/Administrador.js'
import { Cliente } from '../models/Cliente.js'
dotenv.config()


export const loginAdmin = async (req, res) => {
  const { email, senha } = req.body
  const mensaErro = "Erro... E-mail ou senha inválidos"

  if (!email || !senha) {
    res.status(400).json({ erro: "Informe e-mail e senha de acesso" })
    return
  }

  try {
    const administrador = await Admnistrador.findOne({ where: { email } })

    if (administrador == null) {
      res.status(400).json({ erro: mensaErro})
      return
    }

    if (bcrypt.compareSync(senha, administrador.senha)) {
      const token = jwt.sign({
        user_logado_id: administrador.id,
        user_logado_nome: administrador.nome
      },
        process.env.JWT_KEY,
        { expiresIn: "1h" }
      )

      res.status(200).json({ msg: "Administrador logado", id: administrador.id, nome: administrador.nome, token })
      

    } else {
      res.status(400).json({ erro: mensaErro })
    }
  } catch (error) {
    console.log(error);
    res.status(400).json(error)
  }
}

export const loginCliente = async (req, res) => {
  const { email, senha } = req.body
  const mensaErro = "Erro... E-mail ou senha inválidos"

  if (!email || !senha) {
    res.status(400).json({ erro: "Informe e-mail e senha de acesso" })
    return
  }

  try {
    const cliente = await Cliente.findOne({ where: { email } })

    if (cliente == null) {
      res.status(400).json({ erro: mensaErro})
      return
    }

    if (bcrypt.compareSync(senha, cliente.senha)) {
      const token = jwt.sign({
        user_logado_id: cliente.id,
        user_logado_nome: cliente.nome
      },
        process.env.JWT_KEY,
        { expiresIn: "1h" }
      )
      res.status(200).json({ msg: "Cliente logado", id: cliente.id, nome: cliente.nome, token })

    } else {
      res.status(400).json({ erro: mensaErro })
    }
  } catch (error) {
    console.log(error);
    res.status(400).json(error)
  }
}

