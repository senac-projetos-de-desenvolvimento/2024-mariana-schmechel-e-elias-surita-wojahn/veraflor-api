import { sequelize } from '../databases/conecta.js'
import { Cliente } from '../models/Cliente.js';

export const clienteIndex = async (req, res) => {

  try {
    const clientes = await Cliente.findAll();
    res.status(200).json(clientes)
  } catch (error) {
    res.status(400).send(error)
  }
}

export const clienteCreate = async (req, res) => {
  const { nome, email, cpf, dataNasc, area, celular, senha } = req.body

  if ( !nome || !email || !cpf || !dataNasc || !area || !celular || !senha ) {
    res.status(400).json({ id: 0, msg: "Erro... Informe todos os dados." })
    return
  }

  try {
    const clientes = await Cliente.create({
        nome, email, cpf, dataNasc, area, celular, senha
    });
    res.status(201).json(clientes)
  } catch (error) {
    res.status(400).send(error)
  }
}

export const clienteUpdate = async (req, res) => {
  const { id } = req.params
  const { nome, email, cpf, dataNasc, area, celular, senha } = req.body

  if (!nome || !email || !cpf || !dataNasc || !area || !celular || !senha) {
      res.status(400).json({ id: 0, msg: "Atenção! Preencha todos os campos!" })
      return
  }

  try {
      const alteraCliente = await Cliente.update({
        nome, email, cpf, dataNasc, area, celular, senha
      }, {
          where: { id }
      });
      res.status(200).json(alteraCliente)
  } catch (error) {
      res.status(400).send(error)
  }
}

export const clienteDestroy = async (req, res) => {
  const { id } = req.params
  try {
      const deletaCliente = await Cliente.destroy({
          where: { id }
      });
      res.status(200).json(deletaCliente)
  } catch (error) {
      res.status(400).send(error)
  }
}
