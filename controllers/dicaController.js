import { Dica } from "../models/Dica.js";


export const dicaIndex = async (req, res) => {

    try {
        const dicas = await Dica.findAll();
        res.status(200).json(dicas)
    } catch (error) {
        res.status(400).send(error)
    }
}

export const dicaIndexProduto = async (req, res) => {
    const { produto_id } = req.params;

    try {
        const dicas = await Dica.findAll({
            where: { produto_id }
        });
        res.status(200).json(dicas);
    } catch (error) {
        res.status(400).send(error);
    }
};

export const dicaCreate = async (req, res) => {
    const { descricao, nr_regas, exposicao_sol, adubagem, produto_id } = req.body

    if (!descricao || !nr_regas || !exposicao_sol || !adubagem || !produto_id) {
        res.status(400).json({ id: 0, msg: "Erro... Informe todos os dados." })
        return
    }

    try {
        const dicas = await Dica.create({
            descricao, nr_regas, exposicao_sol, adubagem, produto_id
        });
        res.status(201).json(dicas)
    } catch (error) {
        res.status(400).send(error)
    }
}

export const dicasUpdate = async (req, res) => {
    const { id } = req.params
    const { descricao, nr_regas, exposicao_sol, adubagem, produto_id } = req.body

    if (!descricao || !nr_regas || !exposicao_sol || !adubagem || !produto_id) {
        res.status(400).json({ id: 0, msg: "Atenção! Preencha todos os campos!" })
        return
    }

    try {
        const alteraDica = await Dica.update({
            descricao, nr_regas, exposicao_sol, adubagem, produto_id
        }, {
            where: { id }
        });
        res.status(200).json(alteraDica)
    } catch (error) {
        res.status(400).send(error)
    }
}

export const dicaDestroy = async (req, res) => {
    const { id } = req.params
    try {
        const deletaDica = await Dica.destroy({
            where: { id }
        });
        res.status(200).json(deletaDica)
    } catch (error) {
        res.status(400).send(error)
    }
}
