import { Op } from "sequelize";
import { Carrinho } from "../models/Carrinho.js";
import { CarrinhoItem } from "../models/Carrinho_Item.js";
import { Produto } from "../models/Produto.js";

export const carrinhoIndex = async (req, res) => {

    try {
        const carrinho = await Carrinho.findAll();
        res.status(200).json(carrinho)
    } catch (error) {
        res.status(400).send(error)
    }
}

export const carrinhoAtivoIndex = async (req, res) => {
    const { cliente_id } = req.params;

    try {
        const carrinho = await Carrinho.findAll({
            where: {
                cliente_id,
                status: 'Ativo',
            }
        });
        res.status(200).json(carrinho);
    } catch (error) {
        res.status(400).send(error);
    }
}

export const carrinhoIndexCliente = async (req, res) => {
    const { cliente_id } = req.params;

    try {
        const carrinho = await Carrinho.findAll({
            where: { cliente_id }
        });
        res.status(200).json(carrinho);
    } catch (error) {
        res.status(400).send(error);
    }
};

export const carrinhoCreate = async (req, res) => {
    const { status, cliente_id } = req.body

    if (!status || !cliente_id) {
        res.status(400).json({ id: 0, msg: "Erro... Informe todos os dados." })
        return
    }

    try {
        const carrinho = await Carrinho.create({
            status, cliente_id
        });
        res.status(201).json(carrinho)
    } catch (error) {
        res.status(400).send(error)
    }
}


export const cancelarCarrinhosInativos = async (req, res) => {
    try {
        const now = new Date();
        const limite = new Date(now - 1441 * 60 * 1000); // 24 horas e 1 minuto 

        // busca todos os carrinhos que estão "ativos" e foram criados há mais de 24 horas e 1 minuto
        const carrinhosInativos = await Carrinho.update(
            { status: 'Cancelado' },
            {
                where: {
                    status: 'Ativo',
                    createdAt: {
                        [Op.lt]: limite // carrinhos criados antes do limite de 24 horas e 1 minuto
                    }
                }
            }
        );

        return res.status(200).json({ message: `${carrinhosInativos} carrinhos foram cancelados automaticamente` });
    } catch (error) {
        return res.status(500).json({ message: 'Erro ao cancelar carrinhos inativos', error });
    }
};

export const carrinhoDestroy = async (req, res) => {
    const { id } = req.params
    try {
        const deleta = await Carrinho.destroy({
            where: { id }
        });
        res.status(200).json(deleta)
    } catch (error) {
        res.status(400).send(error)
    }
}

// Função auxiliar para calcular o total do carrinho
async function totalCarrinho(id) {
    try {
        const itensCarrinho = await CarrinhoItem.findAll({
            where: { carrinho_id: id },  
            include: [{
                model: Produto,
                attributes: ['preco']  
            }]
        });

        const { total, totalItens } = itensCarrinho.reduce((acc, item) => {
            acc.total += (item.quantidade * item.produto.preco);  
            acc.totalItens += item.quantidade;  
            return acc;
        }, { total: 0, totalItens: 0 });

        return { total, totalItens };  
    } catch (error) {
        throw new Error('Erro ao calcular o total do carrinho');
    }
}

export const obterTotalCarrinho = async (req, res) => {
    const { id } = req.params;
    try {
        const total = await totalCarrinho(id);

        res.status(200).json({
            carrinhoId: id,
            total
        });
    } catch (error) {
        res.status(500).json({ message: 'Erro ao calcular o total do carrinho', error });
    }
};