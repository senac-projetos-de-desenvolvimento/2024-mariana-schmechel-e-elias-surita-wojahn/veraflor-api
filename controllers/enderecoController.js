import { Endereco } from "../models/Endereco.js";

export const enderecoIndex = async (req, res) => {

    try {
        const endereco = await Endereco.findAll();
        res.status(200).json(endereco)
    } catch (error) {
        res.status(400).send(error)
    }
}

export const enderecoIndexCliente = async (req, res) => {

    const { cliente_id } = req.params;

    try {
        const endereco = await Endereco.findAll({
            where: { cliente_id }
        });
        res.status(200).json(endereco);
    } catch (error) {
        res.status(400).send(error);
    }
}

export const enderecoCreate = async (req, res) => {
    const { endereco, numero, complemento, bairro, cidade, estado, cep, cliente_id } = req.body

    if (!endereco || !numero || !complemento || !bairro || !cidade || !estado || !cep || !cliente_id) {
        res.status(400).json({ id: 0, msg: "Erro... Informe todos os dados." })
        return
    }

    try {
        const criaEndereco = await Endereco.create({
            endereco, numero, complemento, bairro, cidade, estado, cep, cliente_id
        });
        res.status(201).json(criaEndereco)
    } catch (error) {
        res.status(400).send(error)
    }
}

export const enderecoUpdate = async (req, res) => {
    const { id } = req.params
    const { endereco, numero, complemento, bairro, cidade, estado, cep, cliente_id } = req.body

    if (!endereco || !numero || !complemento || !bairro || !cidade || !estado || !cep || !cliente_id) {
        res.status(400).json({ id: 0, msg: "Atenção! Preencha todos os campos!" })
        return
    }

    try {
        const alteraEndereco = await Endereco.update({
            endereco, numero, complemento, bairro, cidade, estado, cep, cliente_id
        }, {
            where: { id }
        });
        res.status(200).json(alteraEndereco)
    } catch (error) {
        res.status(400).send(error)
    }
}

export const enderecoUpdateCliente = async (req, res) => {
    const { cliente_id } = req.params
    const { endereco, numero, complemento, bairro, cidade, estado, cep } = req.body

    if (!endereco || !numero || !complemento || !bairro || !cidade || !estado || !cep ) {
        res.status(400).json({ id: 0, msg: "Atenção! Preencha todos os campos!" })
        return
    }

    try {
        const alteraEndereco = await Endereco.update({
            endereco, numero, complemento, bairro, cidade, estado, cep
        }, {
            where: { cliente_id }
        });
        res.status(200).json(alteraEndereco)
    } catch (error) {
        res.status(400).send(error)
    }
}

