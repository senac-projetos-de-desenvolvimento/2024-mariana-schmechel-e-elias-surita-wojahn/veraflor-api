import { DataTypes } from 'sequelize';
import { sequelize } from '../databases/conecta.js';
import { Cliente } from './Cliente.js';
import { Carrinho } from './Carrinho.js';

export const Pedido = sequelize.define('pedido', {
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  total: {
    type: DataTypes.DECIMAL(10, 2),
    allowNull: false
  },
  status: {
    type: DataTypes.STRING(20),
    allowNull: false,
    defaultValue: 'Pendente'
  },
  pagseguro_order_id: {
    type: DataTypes.STRING,
    allowNull: true
  },
  forma_pagamento: {  
    type: DataTypes.STRING(20),
    allowNull: false
  },
  entregue: { 
    type: DataTypes.BOOLEAN,
    allowNull: false,
    defaultValue: false 
  },
  forma_entrega: {  
    type: DataTypes.STRING(8),
    allowNull: false 
  }
}, {
  paranoid: true
});


Pedido.belongsTo(Cliente, {
  foreignKey: {
    name: 'cliente_id',
    allowNull: false
  },
  onDelete: 'RESTRICT',
  onUpdate: 'CASCADE'
});

