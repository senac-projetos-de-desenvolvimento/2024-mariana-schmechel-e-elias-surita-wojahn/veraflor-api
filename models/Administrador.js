import { DataTypes } from 'sequelize';
import { sequelize } from '../databases/conecta.js';
import bcrypt from 'bcrypt'

export const Admnistrador = sequelize.define('administrador', {
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  nome: {
    type: DataTypes.STRING(50),
    allowNull: false
  },
  email: {
    type: DataTypes.STRING(100),
    allowNull: false
  },
  senha: {
    type: DataTypes.STRING(60),
    allowNull: false
  }
}, {
    tableName: "administradores"
});

Admnistrador.beforeCreate(administrador => {
  const salt = bcrypt.genSaltSync(12)
  const hash = bcrypt.hashSync(administrador.senha, salt)
  administrador.senha = hash  
});