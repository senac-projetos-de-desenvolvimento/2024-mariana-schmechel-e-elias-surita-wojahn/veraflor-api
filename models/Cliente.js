import { DataTypes } from 'sequelize';
import { sequelize } from '../databases/conecta.js';
import bcrypt from 'bcrypt'

export const Cliente = sequelize.define('cliente', {
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  nome: {
    type: DataTypes.STRING(50),
    allowNull: false
  },
  email: {
    type: DataTypes.STRING(100),
    allowNull: false
  },
  cpf: {
    type: DataTypes.STRING(14),
    allowNull: false
  },
  dataNasc: {
    type: DataTypes.DATE,
    allowNull: false
  },
  area: {
    type: DataTypes.STRING(2),
    allowNull: false
  },
  celular: {
    type: DataTypes.STRING(9),
    allowNull: false
  },
  senha: {
    type: DataTypes.STRING(60),
    allowNull: false
  }
});

Cliente.beforeCreate(cliente => {
  const salt = bcrypt.genSaltSync(12)
  const hash = bcrypt.hashSync(cliente.senha, salt)
  cliente.senha = hash
});