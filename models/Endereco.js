import { DataTypes } from 'sequelize';
import { sequelize } from '../databases/conecta.js';
import { Cliente } from './Cliente.js';

export const Endereco = sequelize.define('endereco', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    endereco: {
        type: DataTypes.STRING(100),
        allowNull: false,
    },
    numero: {
        type: DataTypes.STRING(10),
        allowNull: false,
    },
    complemento: {
        type: DataTypes.STRING(20),
        allowNull: false,
    },
    bairro: {
        type: DataTypes.STRING(20),
        allowNull: false,
    },
    cidade: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    estado: {
        type: DataTypes.STRING(2),
        allowNull: false,
    },
    cep: {
        type: DataTypes.STRING(9),
        allowNull: false,
    },
});

Endereco.belongsTo(Cliente, {
    foreignKey: {
        name: 'cliente_id',
        allowNull: false,
        unique: true
    },
    as: 'cliente',
    onDelete: 'RESTRICT',
    onUpdate: 'CASCADE'
});

Cliente.hasOne(Endereco, {
    foreignKey: {
        name: 'cliente_id',
        allowNull: false,
        unique: true
    },
    as: 'endereco',
    onDelete: 'RESTRICT',
    onUpdate: 'CASCADE'
});


