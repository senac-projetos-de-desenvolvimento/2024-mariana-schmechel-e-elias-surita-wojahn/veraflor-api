import { DataTypes } from 'sequelize';
import { sequelize } from '../databases/conecta.js';
import { Pedido } from './Pedido.js';

export const Carrinho = sequelize.define('carrinho', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    status: {
        type: DataTypes.STRING(20),
        defaultValue: 'Ativo',
        allowNull: false
    }
}, {
    paranoid: true
});

Carrinho.hasOne(Pedido, {
  foreignKey: {
    name: 'carrinho_id',
    allowNull: false,
    unique: true
  },
  as: 'pedido',
  onDelete: 'RESTRICT',
  onUpdate: 'CASCADE'
});
