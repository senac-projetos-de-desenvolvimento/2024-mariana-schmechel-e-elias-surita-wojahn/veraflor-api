import { DataTypes } from 'sequelize';
import { sequelize } from '../databases/conecta.js';
import { Admnistrador } from './Administrador.js';

export const Produto = sequelize.define('produto', {
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  codigo: {
    type: DataTypes.STRING(15),
    allowNull: false,
    unique: true
  },
  descricao: {
    type: DataTypes.STRING(100),
    allowNull: false
  },
  preco: {
    type: DataTypes.DECIMAL(6, 2),
    allowNull: false
  },
  tipo: {
    type: DataTypes.STRING(20),
    allowNull: false
  },
  imagem: {
    type: DataTypes.STRING,
    allowNull: false
  },
  destaque: {
    type: DataTypes.BOOLEAN,
    allowNull: false
  },
  ambiente: {
    type: DataTypes.STRING(10),
    allowNull: false
  },
  categoria: {
    type: DataTypes.STRING(15),
    allowNull: false
  },
  tamanho: {
    type: DataTypes.STRING(10),
    allowNull: false
  },
  loja: {
    type: DataTypes.STRING(7),
    allowNull: false
  },
  disponivel: {
    type: DataTypes.BOOLEAN,
    allowNull: false,
    defaultValue: true
  }
}, {
  paranoid: true
});

Produto.belongsTo(Admnistrador, {
  foreignKey: {
    name: 'administrador_id',
    allowNull: false
  },
  onDelete: 'RESTRICT',
  onUpdate: 'CASCADE'
});