import { DataTypes } from 'sequelize';
import { sequelize } from '../databases/conecta.js';
import { Carrinho } from './Carrinho.js';
import { Produto } from './Produto.js';

export const CarrinhoItem = sequelize.define('carrinhoItem', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    quantidade: {
        type: DataTypes.INTEGER,
        allowNull: false
    }
}, {
    paranoid: true,
    tableName: "carrinhoItens"
});

