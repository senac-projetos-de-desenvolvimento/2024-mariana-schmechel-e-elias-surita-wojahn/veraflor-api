import { DataTypes } from 'sequelize';
import { sequelize } from '../databases/conecta.js';
import { Produto } from './Produto.js';

export const Dica = sequelize.define('dica', {
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  descricao: {
    type: DataTypes.STRING(600),
    allowNull: false
  },
  nr_regas: {
    type: DataTypes.STRING(50),
    allowNull: false
  },
  exposicao_sol: {
    type: DataTypes.STRING(50),
    allowNull: false
  },
  adubagem: {
    type: DataTypes.STRING(50),
    allowNull: false
  }
}, {
  paranoid: true
});

Dica.belongsTo(Produto, {
  foreignKey: {
    name: 'produto_id',
    allowNull: false,
    unique: true
  },
  as: 'produto',
  onDelete: 'RESTRICT',
  onUpdate: 'CASCADE'
});

Produto.hasOne(Dica, {
  foreignKey: {
    name: 'produto_id',
    allowNull: false,
    unique: true
  },
  as: 'dica',
  onDelete: 'RESTRICT',
  onUpdate: 'CASCADE'
});
