import { Admnistrador } from './models/Administrador.js';
import { Carrinho } from './models/Carrinho.js';
import { CarrinhoItem } from './models/Carrinho_Item.js';
import { Cliente } from './models/Cliente.js';
import { Endereco } from './models/Endereco.js';
import { Pedido } from './models/Pedido.js';
import { Produto } from './models/Produto.js';

Cliente.hasOne(Endereco, {
    foreignKey: 'cliente_id',
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE'
});

Pedido.belongsTo(Carrinho, {
    foreignKey: {
        name: 'carrinho_id',
        allowNull: false,
        unique: true
    },
    as: 'carrinho',
    onDelete: 'RESTRICT',
    onUpdate: 'CASCADE'
});

Carrinho.belongsTo(Cliente, {
    foreignKey: {
        name: 'cliente_id',
        allowNull: false
    },
    onDelete: 'RESTRICT',
    onUpdate: 'CASCADE'
})

Carrinho.hasMany(CarrinhoItem, {
    foreignKey: {
        name: 'carrinho_id',
        allowNull: true
    },
    as: 'carrinhoItens', 
    onDelete: 'CASCADE', 
    onUpdate: 'CASCADE' 
});


CarrinhoItem.belongsTo(Carrinho, {
    foreignKey: {
        name: 'carrinho_id',
        allowNull: false
    },
    onDelete: 'RESTRICT',
    onUpdate: 'CASCADE'
});


CarrinhoItem.belongsTo(Produto, {
    foreignKey: {
        name: 'produto_id',
        allowNull: false
    },
    as: 'produto',
    onDelete: 'RESTRICT',
    onUpdate: 'CASCADE'
});

Produto.belongsTo(Admnistrador, {
    foreignKey: {
      name: 'administrador_id',
      allowNull: false
    },
    onDelete: 'RESTRICT',
    onUpdate: 'CASCADE'
});

Admnistrador.hasMany(Produto, {
    foreignKey: 'administrador_id'
});